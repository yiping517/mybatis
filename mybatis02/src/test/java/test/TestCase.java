package test;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import entity.Emp;
import entity.Employee;

public class TestCase {

	private SqlSession session;
	
	//負責獲得SqlSession對象
	@Before
	public void init() {
			        //配置文件
		String config="SqlMapConfig.xml";
		
		//創建SqlSession對像,以執行sql語句
		//step1.創建SqlSessionFactoryBuilder對像
		SqlSessionFactoryBuilder ssfb = new SqlSessionFactoryBuilder();
		//step2.創建SqlSessionFactory對像
		//SqlSessionFactory ssf = ssfb.build(inputStream);
		//透過輸入流的方式讀取文件
		//Q:怎麼構建這個輸入流呢?
		//Ans:類加載器有個方法:getResourceAsStream(). 它可以構造一個輸入流. 如此,就能通過這個流,讀取文件內容了
		SqlSessionFactory ssf = ssfb.build(TestCase.class.getClassLoader().getResourceAsStream(config));
		//step3.獲得sqlSession對像
		session = ssf.openSession();
	}
	
	@Test
	public void test2() {
		//顧名思義,它返回的是一個list集合
		List<Employee> e = session.selectList("test.findAll" );
		System.out.println(e);
		//今天做的查詢就不用像昨天用spring jdbc那樣,
		//還需要告訴它如何處理結果集.
		//若這個表有幾百個字段,要告訴它一個rowmap,告訴它如何處理結果集,還是比較囉嗦的
		//但現在,就不需要寫了.
		//Q:那它怎麼做到的? 我們並沒有告訴它如何處理結果集阿.
		//像昨天還要寫while(resultSet.next){ //然後對結果集調用getString/getDouble..取出字段值,,然後再賦給實體對象
		//Ans:[擴展昨天的原理圖]
		//昨天說step5. SqlSession依據sql id找到對應的statement,然後調用它的方法來執行.
		//如果是"插入,刪除or修改",直接調用statement的executeUpdate()就行了;
		//若是查詢,就麻煩了一點.因為它涉及要將結果集當中的紀錄轉換成對應的實體對象
		//Q:它是怎麼做的呢?
		//Ans:它分兩步.首先,myBatis會把記錄當中的數據取出來,放到一個map裡面.
		//Q:這個map對象和紀錄是什麼關係呢?
		//Ans:一條紀錄對應一個map對象.
		//Q:對於表來說,這個紀錄當中有什麼東西?
		//Ans:紀錄當中就是字段名和字段值;而map當中有key和value
		//員工表裡有id, name, age這三個字段.令有一條員工紀錄: 1, king, 22 .
		//令myBatis查詢之後,有這麼一條紀錄. Q:它會怎麼處理呢?
		//Ans:它會先把這條紀錄放到一個map裡. 這個map以表的字段名作為key;以字段值為value
		//所以剛剛查出來的那條紀錄,key會是id, name, age; 對應的值會是 1, king, 22
		// =>一條紀錄可以用一個map來表示.
		//老師小結:階段一)若是查詢,會先將紀錄中的數據放到一個map對象裡
		//(以字段名作為key,字段值作為value.一條紀錄對應一個map對象).
		//階段二)再將map對象中的數據放到對應的實體對象裡.
		//**剛強調過:實體類裡的屬性名要和表的字段名一樣.
		//MyBatis會把map當中的數據...map當中的key就是字段名.
		//它就以這個key和實體類中的屬性來一一對應,然後再賦值.
		//這就是為什麼實體類有這麼嚴格的要求(要求屬性名和表的字段名一樣).
		//因為若不一樣,myBatis就找不到這個字段的值,弄不清到底要給哪個屬性,也就無法賦值.
		//完成這兩個階段,才能實現:step6.實體對象
		
		//關閉SqlSession
		session.close();
		//把連接歸還到連接池裡
	}
	
	@Test
	public void test3() {
		//調用SqlSession對象的方法                                               //id值
		Employee e = session.selectOne("test.findById", 1);
		System.out.println(e);
		
		//關閉
		session.close();
	}
	
	@Test
	public void test4() {
		//"修改"需要先把這個員工對像找出來
		Employee e = session.selectOne("test.findById", 3);
		e.setAge(e.getAge()*2);
		session.update("test.modify", e);
		//參數裡有sql的id --- "test.modify"
		//依據這個Sql id, SqlSession就可以找到事先創建好的Statement
		//接著,調用這個statement的executeUpdate() 
		// [preparedStatement ps = ....
		// ps.executeUpdate();]
		//Q:調用上面這個方法前須要幹嘛?
		//Ans: ps.setString()...等. 以給佔位符賦值
		//Q:佔位符來自於哪個地方?
		//Ans:另一個參數 : e
		//Q:這個e怎麼訪問的?
		//Ans:在映射文件EmpMapper.xml中,告訴它
		//要訪問的對象[parameterType="entity.Employee"]
		//及這個對象的getName(), getId(), getAge()
		
		//只要涉及數據的變化[刪除,修改,添加],務必要記得提交事務
		session.commit();
		
		session.close();
	}
	
	@Test
	public void test5() {
		session.delete("test.delete", 2);
		
		//提交事務
		session.commit();
		
		//關閉
		session.close();
		
	}
	
//<!-- springMybatis-day08-04返回map類型的結果 -->		
	@Test
	public void test6() {
		Map data = session.selectOne("test.findById2", 3);
		System.out.println(data);
		//輸出:{ID=3, NAME=學myBatis_Day2_給修改用, AGE=196}
		//Q:有沒有發現,結果裡面字段的屬性名有變化? 都大寫了,為什麼?
		//Ans: MyBatis以字段名為key,字段值為value. 
		//而Oracle數據庫有個特點:建表時,不論字段名是大or小寫,
		//建完後,數據庫存放的字段名都會統一大寫
		//Q:這告訴我們什麼? 若我們單純想輸出員工的姓名,該怎麼寫呢?
		//Ans: map有個get方法吧. 
		//Q:那key應該怎麼填?
		// dat.get("name") ? 
		//Ans:不行. 那樣會取不到值. 因為key是大寫的. 所以:
		System.out.println(data.get("NAME"));
		//老師小結:
		//Oracle數據庫會將字段名統一變大寫形式.
		//這跟數據庫有關. 若換個數據庫,那就不一定了.
		session.close();
	}
	
	//springMybatis-day08-05解決字段名與實體類的屬性名不一致
	//測試情況
	@Test
	public void test7() {
		Emp emp = session.selectOne("test.findById3", 3);
		System.out.println(emp);
		//輸出: Emp [empNo=0, ename=null, age=196]
		session.close();
	}
	
	//
	@Test
	public void test8() {
		Emp emp = session.selectOne("test.findById4", 3);
		System.out.println(emp);
		//輸出:Emp [empNo=3, ename=學myBatis_Day2_給修改用, age=196]
		session.close();
	}
	
}
