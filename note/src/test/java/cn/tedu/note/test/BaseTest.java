package cn.tedu.note.test;

import org.junit.After;
import org.junit.Before;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//public class BaseTest {
public abstract class BaseTest {

	protected ClassPathXmlApplicationContext ctx;
	// "protected"表示給子類用

	public BaseTest() {
		super();
	}

	@Before
	public void initCtx() {
		ctx = new ClassPathXmlApplicationContext(
				"conf/spring-mvc.xml",
				"conf/spring-mybatis.xml",
				"conf/spring-service.xml");
	}

	@After
	public void close() {
		ctx.close();
	}

}