package cn.tedu.note.test;

import org.junit.Before;
import org.junit.Test;

import cn.tedu.note.entity.User;
import cn.tedu.note.service.UserService;

public class UserServiceTest extends BaseTest {
//cloudNote-day03-04 註冊功能3------------------始
	UserService service;
	
	@Before
	public void initService() {
		service = ctx.getBean("userService", UserService.class);
	}
	
	@Test
	public void testRegist() {
		User user = service.regist("Andy", "Andy", "123456", "123456");
		System.out.println(user);
	}
	
//------------------------cloudNote-day03-04 註冊功能3-----末	
	@Test
	public void test() {
		String name="demo";
		String password="123456";
		User user = service.login(name, password);
		System.out.println(user);
	}
	//輸出結果 : cn.tedu.note.service.PasswordException: 密碼錯誤
}
