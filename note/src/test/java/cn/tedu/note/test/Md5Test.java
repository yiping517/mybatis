package cn.tedu.note.test;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

public class Md5Test {

	@Test
	public void testMd5() {
		String str = "123456";
		String md5 = DigestUtils.md5Hex(str);
		System.out.println(md5);
		
		//加鹽摘要
		String salt = "今天你吃了嗎?";
		md5 = DigestUtils.md5Hex(salt+str);
		System.out.println("加鹽後的:"+md5);
		
		//update cn_user
		//set cn_user_password = '4f62e9ccea00750aaab5b2d38437c3f1'
		//where cn_user_name='demo';
		//Q:若執行上面這條SQL意味什麼?
		//OS:將demo用戶的密碼改成經md5加鹽加密後的123456.
		//師:如此,現在要比較密碼便不能再比較明文了.
		//只能比加鹽後的密文.
		//Q:如何比較加鹽後的密文?
		// --------- note項目下的UserServiceImpl.java
	}

}
