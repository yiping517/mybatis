package cn.tedu.note.test;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import cn.tedu.note.dao.NoteDao;
import cn.tedu.note.entity.Note;

public class NoteDaoTest extends BaseTest{
	NoteDao dao;
	
	@Before
	public void initDao() {
		dao = ctx.getBean("noteDao", NoteDao.class);
	}
	
	@Test
	public void testFindNotesByNotebookId() {
		String id = "8844559e-9d93-459b-a9db-2afcb8a592c8";
		List<Map<String,Object>> list = dao.findNotesByNotebookId(id);
		for(Map<String,Object> li : list) {
			System.out.println(li);
		}
		//以前的方式:映射到實體,通過實體讀數據
		//但用實體不好. Q:why?
		//用實體時,抓到的數據量太大了.
		//Q:什麼意思?
		//Ans:cn_note這個表並不是只有title,還有body,notebookId..等
		//都映射好以後,發到介面上. 而我們只顯示title. body,notebookId等都用不著
		//這樣會造成訪問量的浪費. 非常浪費流量.
		//網站的下載流量是會收費的.如:咱看京東網頁時,京東需要給電信交錢.
		//雖說1G幾分錢. 但它們量大. 一天幾百個G, 甚至多個T
		//如此大的下載流量. 省一點是一點.
		//**所以像京東,掏寶專門有個叫"流量優化"的部門
		//它們就是想盡一切辦法,減少網頁的下載量. 提高網站的效率.
		//對咱來說,我們從數據庫抓數據,最後通過控制器傳遞到Browser. 
		//Q:從數據庫抓數據,最後從控制器傳遞到瀏覽器,這個地方要不要經過互聯網? 
		//Ans:要.因為Controller在Server; 網頁在Browser. 中間走的就是互聯網.
		//若拿實體映射,將實體數據順著網路發過去,那流量很大的.
		//但現在用Map. Map抓多少,發多少. 只發送我們所選擇的那些. 
		//如此,發送的就少. 流量節省. 網站響應速度快. 用戶體驗就好了.
	}
	
	@Test
	public void testCountNotebookById() {
		String id="d0b0727f-a233-4a1f-8600-f49fc1f25bc9";
		int a = dao.countNotebookById(id);
		System.out.println(a);
	}
	
	@Test
	public void TestAddNote() {
		Note note = new Note();
		String noteId = UUID.randomUUID().toString();
		note.setId(noteId);
		String notebookId = "0b11444a-a6d6-45ff-8d46-282afaa6a655";
		note.setNotebookId(notebookId);
		String userId="52f9b276-38ee-447f-a3aa-0d54e7a736e4";
		note.setUserId(userId);
		String statusId="1";
		note.setStatusId(statusId);
		String typeId="0";
		note.setTypeId(typeId);
		String body = "測試1106";
		note.setBody(body);
		String title="1106測試";
		note.setTitle(title);
		Long createTime=(long) 11061106;
		note.setCreateTime(createTime);
		Long modifyTime = (long) 11061932;
		note.setLastModifyTime(modifyTime);
		int r = dao.addNote(note);
		System.out.println(r);
	}
}
