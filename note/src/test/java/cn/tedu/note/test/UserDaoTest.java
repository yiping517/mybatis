package cn.tedu.note.test;

import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.tedu.note.dao.UserDao;
import cn.tedu.note.entity.User;

public class UserDaoTest extends BaseTest{
	
//ㄅ2
	UserDao dao;
	@Before
	public void initDao() {
		 dao = ctx.getBean("userDao", UserDao.class);
	}

//	ClassPathXmlApplicationContext ctx;
//	
//	@Before//測試之前,把容器打開.
//	public void initCtx() {
//		ctx = new ClassPathXmlApplicationContext(
//				"conf/spring-mvc.xml","conf/spring-mybatis.xml");
//					//初始化spring容器時,是可以加載多個配置文件的.
//		
//	}
//	
//	@After//測試之後,把容器關了.
//	public void closeCtx() {
//		ctx.close();
//	}
	
	@Test
	public void testFindUserByName() {
		String name="demo";
//ㄅ1-----------------------------------始		
//		UserDao dao = ctx.getBean("userDao", UserDao.class);
//--------------------------------------末		
					//從容器中獲取一個bean對象
		//Q:這個bean對象是spring容器創建的?還是mybatis-spring插件創建的?
		//Ans:後者裡面有個MapperScannerConfigurer類. 是它創建的.
		//它掃描我們Dao的接口包. 它掃描的時候創建的.
		User user = dao.findUserByName(name);
		System.out.println(user.toString());
	}
	
		//cloudNote-day03-02 註冊功能1
		@Test
		public void testAddUser() {
			//至此,又需要再創建一個UserDao對象
			// =>代碼與上段重複
			// =>重構---------->ㄅ1
			
			//此處id並不用數據庫生成,咱自己生成一個
			//Q:那如何能生成一個世上獨一無二的id?
			//Ans: java給了一個算法---UUID
			String id = UUID.randomUUID().toString();
			//經常拿這個生成的字符串當id用,避免重複
			
			String name  = "Tom";
			String salt = "今天你吃了嗎?";
			String password = DigestUtils.md5Hex(salt+"123456");
			String token = "";
			String nick = "";

			User user = new User(id,name,password,token,nick);
			int n = dao.addUser(user);//n代表新增了幾筆
			System.out.println(n);
			
		}
	
}

















