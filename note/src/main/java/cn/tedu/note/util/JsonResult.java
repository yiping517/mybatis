package cn.tedu.note.util;

import java.io.Serializable;

public class JsonResult implements Serializable {

	private int status; 
	//為了避免忘記status值所代表的意思,所以:
	public static final int SUCCESS = 0;
	public static final int ERROR = 1;
	
	private String message;//錯誤訊息
	
//正確時的數據----------------------------------
	private Object data;
	
	//重載Constructor:
	public JsonResult(Object data) {
		status = SUCCESS;
		this.data = data;
	}
//----------------------------------------
//	Q:這麼做有什麼好處?
//	Ans:統一了返回值.
//	測試. 畫面只出現: {"status":0,"message":null}
//	因為忘了加get和set. 變成JsonResult時,轉換成json,
//	是利用get和set方法轉換.=====>看ㄍㄍㄍ
	
	public JsonResult() {
		super();
	}
	
	//再給個重載(overload)的Constructor:
	//此處參數用父類型定義,方便多態處裡.
	public JsonResult(Throwable e) {
		status = ERROR;
		message = e.getMessage();
		
	}
	
//+++++++ cloudNote-day03-01 登陸功能12 +++++++++
	public JsonResult(int status, Throwable e) {
		this.status = status;
		this.message = e.getMessage();
	}
// +++++++++++++++++++++++++++++++++++++++++++++++
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
//	ㄍㄍㄍ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "JsonResult [status=" + status + ", message=" + message + ", data=" + data + "]";
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~止
//	此時,畫面出現:
//	{"status":0,"message":null,"data":{"id":"48595f52-b22c-4485-9244-f4004255b972","name":"demo","password":"4f62e9ccea00750aaab5b2d38437c3f1","token":null,"nick":null}}
//因此,若我們要抓這個有效數據,那就 .data就可以了.
//要檢查狀態,就是 .status
	
	
}
