package cn.tedu.note.service;

import cn.tedu.note.entity.User;

/**
 * 業務層接口
 *
 */
public interface UserService {

	//登陸功能. 成功就返回用戶信息
	User login(String name, String password) throws UserNotFoundException, PasswordException;

	/**
	 * 註冊功能
	 * @param name
	 * @param password
	 * @param nick
	 * @param confirm
	 * @return 註冊成功的用戶信息
	 * @throws UserNameException 用戶名異常
	 * @throws PasswordException 密碼異常
	 */
	User regist(String name,String nick, String password, String confirm) throws UserNameException, PasswordException;
}
