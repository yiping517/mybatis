package cn.tedu.note.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.tedu.note.dao.NotebookDao;
import cn.tedu.note.dao.UserDao;
import cn.tedu.note.entity.User;
import cn.tedu.note.service.NotebookService;
import cn.tedu.note.service.UserNotFoundException;

@Service
// =>出現錯誤訊息:org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'notebookService' is defined
//@Service("notebookService")
public class NotebookServiceImpl implements NotebookService {

	@Resource
	private NotebookDao notebookDao;
	
	@Autowired
	private UserDao userDao;
	
	public List<Map<String, Object>> listNotebooks(String userId) throws UserNotFoundException {
		
//		return notebookDao.findNotebookByUserId(userId);
		//如此,程序就能用了.
		//但若userId是空,就會出現異常. 所以:
		if(userId == null || userId.trim().isEmpty()) {
			throw new UserNotFoundException("ID不能空");
		}
		User user = userDao.findUserById(userId);
		if(user==null) {
			throw new UserNotFoundException("用戶不存在");
		}
		return notebookDao.findNotebookByUserId(userId);
	}

}
