package cn.tedu.note.service.impl;

import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cn.tedu.note.dao.UserDao;
import cn.tedu.note.entity.User;
import cn.tedu.note.service.PasswordException;
import cn.tedu.note.service.UserNameException;
import cn.tedu.note.service.UserNotFoundException;
import cn.tedu.note.service.UserService;

//Q:如何把這個類變成bean組件,交給spring管理?
//Ans:加註解
@Service("userService")
//Q:這個@Service現在有被掃瞄到嗎?
//Ans:沒有.因為沒有組件掃描
public class UserServiceImpl implements UserService {

	//把salt變成UserService的實例變量
	//String salt = "今天你吃了嗎?";
	//但是,這樣也不好. 所以:
	@Value("#{jdbc.salt}")
	//=>把salt寫到properties文件 =>改配置文件
	private String salt;
	//改好以後,進行業務層測試~~~
	
//ㄅㄅㄅ	
	@Resource //=>對象注入 ; @Value是值注入,可以幫讀properties文件
	UserDao userDao;
	
	public User login(String name, String password) throws UserNotFoundException, PasswordException {

//ㄆㄆㄆ--------------------------------
		if(password==null || password.trim().isEmpty()) {
			throw new PasswordException("密碼為空");
		}
		//Q:或運算兩端可以交換順序嗎?
		//OS:可以
		//師:不可以,因為交換了,就存在空指針異常的風險了.
		//當password為空,直接掉用trim()...當引用類型變量為空,訪問其屬性or方法,就會出現空指針異常
		//而現在這種寫法,因為捷徑運算,所以當第一個條件滿足,短路了,
		//也就不會執行後面,所以空指針異常的風險就跳過去了.
		//所以,此處一定不能交換順序.
//-----------------------------------
		if(name==null || name.trim().isEmpty()) {
			throw new UserNotFoundException("用戶名為空");
		}
		
		//調用
		User user = userDao.findUserByName(name);
		//變量所對應的對象有3種來源:
		//1.自己new 2.從工廠獲得  3.注入==>看ㄅㄅㄅ
		if(user==null) {
			throw new UserNotFoundException("name錯誤");
		}
//ㄗㄗㄗ --------------------------------------------------------
//		String salt = "今天你吃了嗎?";
		String pwd = DigestUtils.md5Hex(salt+password.trim());
		if(pwd.equals(user.getPassword())) {
//		if(password.trim().equals(user.getPassword())) {
//--------------------------------------------------------------		
			//Q:為什麼要trim()?
			//Ans:避免用戶輸入有空格
			return user;
		}
//		cloudNote1-day02-05 信息摘要2
//		數據庫改完以後,拿回來的是密文
//		因為數據庫存的是密文
//		而上方的password.trim()拿到的是明文
//		Q:如何將之轉成密文?
//		看上方ㄗㄗㄗ
		throw new PasswordException("密碼錯誤");
	}
	//至此,程序有bug. 
	//當password是null時,會出現NullPointerException
	//任何時候,只要程序中有引用類型變量. 
	//就要想清楚它什麼時候賦的值
	//Q:何時出空指針異常?
	//Ans:當引用類型變量其值為空的時候,我們訪問其屬性or方法將出現空指針異常.
	//注意!!!!不許說"一個對象是空的時候". 
	//因為,既然已經說是對象了,那對象就一定存在,不可能為空.
	//Q:還有,上面寫的User, password,他們是對象,還是引用對象的變量?
	//Ans:引用對象的變量.
	//所以,在java中,根本就不存在對象為空這件事.
	//java中存在的是,引用類型變量的值是空的.
	
	//Q:如何解決?
	//Ans:判斷一下----看ㄆㄆㄆ
	
//cloudNote-day03-05 註冊功能4	
//	public User regist(String name, String password, String nick, String confirm)
//			throws UserNameException, PasswordException {
	//此時,畫面出現 : {"status":3,"message":"密碼不一致","data":null}
	public User regist(String name, String nick, String password, String confirm)
			throws UserNameException, PasswordException {
	//此時,畫面出現 : {"status":0,"message":null,"data":null}	
		
//ㄙㄙㄙ-----------------------
	//檢查name:
	//1.不能空	
	if(name==null || name.trim().isEmpty()) {
		throw new UserNameException("不能空");
	}
	//2.不能重複
	//Q:怎麼檢查重複否?
	//Ans:必須去數據庫裡檢查. 所以調用方法
	User one = userDao.findUserByName(name);
	if(one != null) {
		//表示數據庫裡已有此人,所以
		throw new UserNameException("已註冊");
	}
	
	//檢查密碼:
	//1.不能空
	if(password == null || password.trim().isEmpty()) {
		throw new PasswordException("密碼不能空");
	}
	
	//2.檢查代碼是否一樣
	if( !password.equals(confirm)) {
		throw new PasswordException("密碼不一致");
	}
	
	//檢查nick
	if(nick == null || nick.trim().isEmpty()) {
		nick = name; 
	}
	//盡量少拋異常. 能處理的,盡量處理.
	//減少異常的目的:提高用戶體驗.
		
		//思考:
		//1.最終要調用:
		//userDao.addUser(user);
		//但現在有個編譯錯誤,缺user對象
		//2.所以,咱創建一個:
	String id = UUID.randomUUID().toString();
//	String salt = "今天你吃了嗎?";
	password = DigestUtils.md5Hex(salt+password);
	String token = "";
	//name雖然已經傳進來了. 但有些問題:不能為空,不能重複.
	//所以需要檢驗----->ㄙㄙㄙ
	
		User user = new User(id,name,password,token,nick);
		int n = userDao.addUser(user);
		if(n != 1) {
			throw new RuntimeException("新增失敗");
		}
//		return null;
		return user;
	}
	//寫到這兒發現,salt在好多處都重複了. 
	//	=>相當不好.
	//	=>將salt挪到class裡的最上方
}










































