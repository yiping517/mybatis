package cn.tedu.note.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.tedu.note.dao.NoteDao;
import cn.tedu.note.dao.NotebookDao;
import cn.tedu.note.entity.Note;
import cn.tedu.note.service.NoteService;
import cn.tedu.note.service.NotebookNotFoundException;
import cn.tedu.note.service.UserNotFoundException;

@Service("noteService")
public class NoteServiceImpl implements NoteService {

	@Resource
	private NoteDao dao;
	
	@Autowired
	private NotebookDao notebookDao;
	
	public List<Map<String, Object>> listNotes(String notebookId) throws NotebookNotFoundException {

		if(notebookId == null || notebookId.trim().isEmpty()) {
			throw new NotebookNotFoundException("無notebookId");
		}
		/**
		Notebook noteBook = dao.findNotebookById(notebookId);
		//此處返回Map可以.
		//但,一般情況下,別什麼都用Map. 
		//前面用Map的目的是為了,減少查詢量.
		//減少數據庫到客戶端的數據傳送量.
		//但此處就是找一個筆記本. 可以用實體.
		if(notebook == null) {
			throw new NotebookNotFoundException("");
		}
		*/
		//但若說:此作法從數據庫,查出所有筆記本的訊息,量特別大,
		//且這個屬性就是驗證它有沒有. 
		//那些屬性都沒用上
		//映射這一下不就慢了.
		//若嫌性能不好,還能重構成:
		
		  int n = dao.countNotebookById(notebookId);
		  System.out.println("n-------------------------"+n);
		  if( n!= 1){
		  	throw new NotebookNotFoundException("找不到notebook");
		  }
		 
		return dao.findNotesByNotebookId(notebookId);
	}

	public Note addNotes(String title, String userId, String notebookId)
			throws NotebookNotFoundException {
		
		//驗證筆記本 Id
		if(notebookId == null || notebookId.trim().isEmpty()) {
			throw new NotebookNotFoundException("無notebook Id");
		}
		
		//驗證user Id
		if(userId == null || userId.trim().isEmpty()) {
			throw new UserNotFoundException("無user Id");
		}
		
		Note note = new Note();
		String id = UUID.randomUUID().toString();
		note.setId(id);
		note.setNotebookId(notebookId);
		note.setUserId(userId);
		note.setStatusId("1");
		note.setTypeId("0");
		note.setTitle(title);
		note.setBody("");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		System.out.println(sdf.format(new Date()));
		System.out.println((sdf.format(new Date())).toString());
		Long createTime = Long.valueOf((sdf.format(new Date())).toString());
		note.setCreateTime(createTime);
		note.setLastModifyTime(createTime);
		
		int r = dao.addNote(note);
		
		if(r>0)
			return note;
		return null;
	}
	
	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		System.out.println(sdf.format(new Date()));
		System.out.println((sdf.format(new Date())).toString());
		Long createTime = Long.valueOf((sdf.format(new Date())).toString());
		System.out.println(createTime);
	}

}
