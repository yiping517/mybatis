package cn.tedu.note.service;

import java.util.List;
import java.util.Map;

import cn.tedu.note.entity.Note;

public interface NoteService {
	
	List<Map<String,Object>> listNotes(String notebookId) throws NotebookNotFoundException;

	Note addNotes(String title, String userId, String notebookId) throws NotebookNotFoundException;
}
