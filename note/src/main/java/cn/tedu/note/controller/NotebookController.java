package cn.tedu.note.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tedu.note.service.NotebookService;
import cn.tedu.note.util.JsonResult;

@Controller
@RequestMapping("/notebook")
public class NotebookController extends AbstractController {

	@Resource
	private NotebookService notebookService;
	
	@ResponseBody
	@RequestMapping("/list.do")
	//以下3種寫法亦可:
	//@RequestMapping("list.do")
	//@RequestMapping("/list")
	//@RequestMapping("list")
	public JsonResult list(String userId){
		List<Map<String,Object>> list = notebookService.listNotebooks(userId);
		return new JsonResult(list);
	}
	//@ExceptionHandler(Exception.class)
	//@ResponseBody
	//public JsonResult handleException(Exception e) {
	//	e.printStackTrace();
	//	return new JsonResult(e);
	//}
	//Q:這麼做好不好?
	//Ans:不好. 因為代碼冗餘了. =>上面這段代碼在UserController也有
	//	=>多個類都有的東西,抽到父類裡
	//	=>重構
	//	=>"Refractor"->"Extract SuperClass"
	
}
