package cn.tedu.note.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tedu.note.entity.User;
import cn.tedu.note.service.PasswordException;
import cn.tedu.note.service.UserNameException;
import cn.tedu.note.service.UserNotFoundException;
import cn.tedu.note.service.UserService;
import cn.tedu.note.util.JsonResult;

@Controller 
//可以換成@Component,亦能換成@Service
//但從業務角度,建議就用@Controller.
//既然是 控制器,還有另一個重要的註解:
@RequestMapping("/user")
public class UserController extends AbstractController {
	
//ㄅㄅㄅ.注入業務層----------------------
	@Resource
	//寫@Autowired亦可
	private UserService userService;
//--------------------------------------	
	
	//Q:控制器方法參數要和誰對應?
	//Ans: Browser提交的參數的名字
	//此處寫name,password就意味著,一會兒請求控制器方法的時候,,
	//要提供兩參:name和password
	@RequestMapping("/login.do")
	//由於此處要採用json, ajax的工作方式,
	//Q:所以此處要採用哪個註解?
	@ResponseBody
	public Object login(String name, String password) {
/**
		//思考幾個問題:
		//1.要調用業務層的哪個方法?
		//Ans: login()
		//既然要調用業務層,必須要能夠訪問業務層組件
		//所以,必須注入業務層----->看ㄅㄅㄅ
//07 登陸功能7===========================================		
		User user = userService.login(name, password);
		return user;
		//寫完以後,測試:將server run起來
		//若Console出現:Mapped "{[/user/login.do],
		//說明此Controller佈署成功了
		//接著,在Browser url處輸入: http://localhost:8082/note/user/login.do?name=demo&password=123456
//===================== 
		try {
			User user = userService.login(name, password);
	// ㄘㄘㄘ ----------------------始
			return user;
	//-----------------------------末
	return new JsonResult(user);
	//===>重構JsonResult,在它裡面再加個屬性
		}catch(Exception ex) {
			ex.printStackTrace();
			//此處沒有返回值,
			//意味著,程序走到這,出異常的時候沒有返回值
			//所以:
		//	return ex.getMessage();
		//此時,當出錯時,畫面出現: ????
		//編碼問題!!!!
		//即便出現4個中文,程序也不好判斷.
		//Q:程序中用什麼代表狀態更好呢?
		//Ans: 數字
		//但是,錯誤訊息也是要告訴用戶. 咱返回2個數據,
		//一個告訴用戶結果是正常or異常;第2個是錯誤消息
		//Q:那要從Server帶過去2個以上的數據,用什麼來處理?
		//OS: json
		//師: map
//		Map<String,Object> map = new HashMap<String,Object>();	
//		map.put("status", "1");//表示有異常
//		map.put("message", ex.getMessage());
		//前次直接返回字符串時,因為字符串不能直接做為json對象
		//所以,返回的結果沒有編碼
		//但此處用map當作返回對象時,
		//springMVC的那一套jackson會自動幫我們處理這個數據的編碼
//		return map;
//記住!!!!!!!!!!!!
//使用json返回數據時,若只一次,那使用map就挺好
//但若是需要反覆從Server端向client端帶東西,每次都用map會顯得非常囉嗦
//Q:怎辦?
//Ans:返回數據除了可以使用map,還能使用對象.
//且map和對象對Client端來說,是一樣的.
//**對象自然是javaBean對象
//所以,可以做個javaBean對象,裡面有2屬性:status, message
return new JsonResult(ex);
}
//再次強調: 
//Q:何時用對象封裝?
//Ans:反覆用時.

//但此時,還有問題:正確和錯誤時的返回值不統一.
//錯誤的檢查已寫在上方.
//Q:但正確時,怎麼檢查?什麼時候正確?
//為了在客戶端既能檢查正確,也能檢查錯誤===>看ㄘㄘㄘ

*以上這些,當catch又分很多級時,顯得很噁心.
*所以採用以下另一種方法:
*/
		User user = userService.login(name, password);
		return new JsonResult(user);
		
	}
//cloudNote-day03-08 註冊功能7---------------------始
	@ExceptionHandler(UserNameException.class)
	@ResponseBody
	public JsonResult handleUserName(UserNameException e) {
		e.printStackTrace();
		return new JsonResult(4,e);
	}
//cloudNote-day03-08 註冊功能7-----------------末	
					   //異常類型 =>表示處理Exception類型的異常.
 //cloudNote-day04-02 筆記本列表4 -----重構始--------
 //	@ExceptionHandler(Exception.class)
	//Q:這個@ExceptionHandler如何執行呢?
	//Ans:其他控制器方法執行出限異常時,執行這個方法 
	//這個就是用來替代掉剛剛前面寫的tryCatch
 //	@ResponseBody
	//在這個裡面,方法隨便寫.
 //	public Object handleException(Exception e) {
		
 //		e.printStackTrace();
 //		return new JsonResult(e);
 //	} cloudNote-day04-02 筆記本列表4 -----重構尾--------
	
	//cloudNote-day03-01 登陸功能12
	@ExceptionHandler(UserNotFoundException.class)
	@ResponseBody
	public JsonResult handleUserNotFound(UserNotFoundException e) {
		e.printStackTrace();
		return new JsonResult(2,e);
		//=>重構JsonResult. 多建一個Constructor
	}
	
	//現在有多個處理異常的ExceptionHandler
	//有具體(UserNotFound),有抽象
	//程序出異常時,會盡可能先調用具體的異常處理
	//有UserNotFoundException會先調UserNotFoundException
	//再調用Exception
	
	@ExceptionHandler(PasswordException.class)
	@ResponseBody
	public JsonResult handlePassword(PasswordException e) {
		e.printStackTrace();
		return new JsonResult(3,e);
	}
	
// cloudNote-day03-05 註冊功能4 ---------------------始
	@RequestMapping("/regist.do")
	@ResponseBody
	public JsonResult regist(String name,String nick,String password,String confirm) {
		User user = userService.regist(name, nick, password, confirm);
		return new JsonResult(user);
		//Q:這麼寫完之後,有異常嗎?
		//Ans:有.
		//Q:處理了嗎?
		//Ans:處理了.昨天寫的,就處理了. 所以,至此就完了. =>發布測試.
		//訪問: localhost:8082/note/user/regist.do?name=Jerry&nick=AN&password=12345&confirm=12345
	}
	
//---------------------------------cloudNote-day03-05 註冊功能4----末	
	
}
