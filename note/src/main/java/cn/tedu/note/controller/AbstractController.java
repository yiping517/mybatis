package cn.tedu.note.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tedu.note.util.JsonResult;

public abstract class AbstractController {

	/**
	 * 在其他控制器方法執行出現異常時,
	 * 執行異常處理方法
	 */
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public JsonResult handleException(Exception e) {
		e.printStackTrace();
		return new JsonResult(e);
	}
	//Q:這麼做好不好?
	//Ans:不好. 因為代碼冗餘了. =>上面這段代碼在UserController也有
	//	=>多個類都有的東西,抽到父類裡
	//	=>重構
	//	=>"Refractor"->"Extract Class"

}