package cn.tedu.note.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tedu.note.entity.Note;
import cn.tedu.note.service.NoteService;
import cn.tedu.note.util.JsonResult;

@Controller
@RequestMapping("/note")
public class NoteController extends AbstractController{

	@Resource
	private NoteService noteService;
	
	@RequestMapping("/list.do")
	@ResponseBody
	public JsonResult list(String notebookId) {
		System.out.println("NoteController中的list()");
		List<Map<String,Object>> list = noteService.listNotes(notebookId);
		return new JsonResult(list);
	}
	
	@RequestMapping("/add.do")
/**	我寫的:
	public Note add(String title, String userId, String notebookId) {
		
		Note note = noteService.addNotes(title, userId, notebookId);
		
		return note;
	}
*/
	//老師:
	@ResponseBody
	public JsonResult add(String userId, String notebookId, String title) {
		System.out.println("進到NoteController的add()");
		
		Note note = noteService.addNotes(title, userId, notebookId);
		return new JsonResult(note);
	}
	
}

	