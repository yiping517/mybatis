package cn.tedu.note.dao;

import cn.tedu.note.entity.User;

public interface UserDao {

	User findUserByName(String name);
	//實體類一定要跟數據庫的表有關.
	//表有幾個屬性,實體類就要有幾個屬性
	
	//cloudNote-day03-02 註冊功能1
	int addUser(User user);

	User findUserById(String userId);
}
