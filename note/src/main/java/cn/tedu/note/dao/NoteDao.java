package cn.tedu.note.dao;

import java.util.List;
import java.util.Map;

import cn.tedu.note.entity.Note;

public interface NoteDao {

	List<Map<String,Object>> findNotesByNotebookId(String notebookId);

	int countNotebookById(String notebookId);
	
	int addNote(Note note);
}
