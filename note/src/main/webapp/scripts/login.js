/*  scripts/login.js 編碼為utf-8 */
$(function(){
	//測試語句
	//測試的目的有2:
	//1. jQuery是否成功加載進來
	//2. login.js有沒有成功在網頁中被加載,被使用
	console.log("hello world!");
	//javascript較容易出錯,所以,寫一點,就測一點.
	//測試:重新佈署. 訪問: localhost:8082/note/log_in.html
	
	//使用id選擇器,找到登陸按鈕
	$('#login').click(loginAction);
				//註冊事件. 將事件請求轉發給loginAction方法
				// =>表示,當點了註冊按鈕後,將執行loginAction()
				//注意!!!!!!!!!!此處千萬不能加(). 千萬不能寫成loginAction()
				//因為,加()表示執行方法. 不加()才表示事件綁定
	
	//ㄅ1
	$('#count').blur(checkName);
	//Q:提交時檢驗,失去焦點也要檢驗. 怎辦?
	//Ans:抽出來,封裝成一個方法
	
	$('#password').blur(checkPassword);
	
//cloudNote-day03-05 註冊功能4-----------------------始
	//使用$函數找到對象
	$('#regist_button').click(registAction);
	//亦即,點擊註冊按鈕時,執行registAction方法
	
	$('#regist_username').blur(checkRegistName);
	
//cloudNote-day03-06 註冊功能5 
	$('#regist_password').blur(checkRegistPassword);
	
	$('#final_password').blur(checkConfirm);
	
});

function checkConfirm(){
	var confirm = $('#final_password').val();
	var pwd = $('#regist_password').val();
	if(pwd && confirm == pwd){
		//javascript中,空串相當於false. 所以,此處判斷
		//password有無輸入. 輸入,pwd則為true ;空值,則為false
		
		//關掉消息
		$('#final_password').next().hide();
		return true;
	}
	$('#final_password').next().show().find('span').html('密碼不一致');
	return false;
}

function checkRegistPassword(){
//建議方法名就長一點.避免衝突. 尤其是javascript	
	console.log("checkRegistPassword");
	var pwd = $('#regist_password').val().trim();
	var rule = /^\w{4,10}$/;
	if(rule.test(pwd)){
		$('#regist_password').next().hide();
		return true;
	}
	$('#regist_password').next().show().find('span').html('4~10個字符');
	return false;
}

//ㄛㄛㄛ
function checkRegistName(){
	var name = $('#regist_username').val().trim();
	var rule = /^\w{4,10}$/;
	if(rule.test(name)){
		//$('#regist_username').next().find('span').empty();
		$('#regist_username').next().hide();
		return true;
	}
//ㄔㄔㄔ	
	//$('#regist_username').next().find('span').html('4~10字符');
	$('#regist_username').next().show().find('span').html('4~10字符');
	return false;
}
//上段程式碼用了個比較古怪的查詢:
//咱看一下log_in.html可以發現,id為regist_username的輸入框後,緊接著個div
//div裡有個span. 這是我們要顯示錯誤提示的地方
//所以,我們的目標是div裡的span元素
//Q:如何找到div呢?
//Ans: next()
//Q:怎麼找到div裡的span呢?
//Ans:用find()

//另外還有個問題:那個span默認不顯示. 我們得把它顯示出來---->ㄔㄔㄔ

function registAction(){
	console.log('registAction');
	//提交表單之前,再檢驗介面參數
	var n = checkRegistName() + checkRegistPassword() + checkConfirm();
	if(n!=3){
		return;
	}
		//看ㄛㄛㄛ
	
	//先拿到介面中的表單數據
	var name = $('#regist_username').val().trim();
	console.log(name);
	var password = $('#regist_password').val();
	var nick = $('#nickname').val();
	var confirm = $('#final_password').val();
	
	//發起ajax請求
		//$.post();
		// $.ajax()的簡化版.它的底層就是$.ajax();
		//但它用起來比$.ajax();簡單. 
		//$.ajax()要指定一堆參數; $.post()指定3參[url,data,function(result){}]就行了
		//不用指定發送數據類型,因為$.post()就是post請求
		//即: $.post(url,data,function(result){});
		//result是Server送回來的. 即Server上的JsonResult
		var url = 'user/regist.do';
		var data = {name:name, 
				nick:nick,
				password:password,
				confirm:confirm};
		//與Server接收的數據有關. Q:Server接收幾個參數呢?
		// :左邊的key要與Server上的參數一致; 而後面的值是變量名(即上邊的"介面中的表單數據")
		$.post(url,data,function(result){
			//先測試. 應顯示:表單點"註冊"後,Server送回一個"創建成功"的用戶訊息
			console.log(result);
			if(result.status == 0){
				//退回登錄介面
				//Q:怎麼退回?
				//Ans:不能用上一頁,因為它是一個介面,有2個表單
				//Q:怎辦?
				//Ans:log_in.html裡有個id是back的鈕.
				//它上面綁定了個事件((拉到log_in.html最下面,可以看到:
				/*
				 * get('back').onclick=function(){
				 * 		get('zc').className='sig sig_out';
				 * 		get('dl').className='log log_i';
				 * }
				 */
				//咱執行上面那兩個get方法也可以,
				//或乾脆執行這個事件. 對back發起一個點擊事件. 讓它執行裡面的兩個方法
				//先找到back按鈕
				$('#back').click();
				//click()方法可以用來綁定事件,也可以發起事件.
				//調用click(),不給參數就是發起事件.發起一個對back按鈕的點擊事件
				
				//為了更人性化一點:註冊完了,用戶名也有了.想幫用戶省事:
				//幫用戶填好用戶名,密碼讓用戶自己填
				//Q:怎做?
				//Ans:找到用戶名
				//Q:用戶名在哪?
				//Ans:在結果中
				var name = result.data.name;
				//result.data就是註冊成功的訊息. 這是Server送回來的
				
				$('#count').val(name);
				
				//促發獲得焦點事件:讓光標在密碼框閃爍,讓用戶敲密碼
				$('#password').focus();
			
				//註冊成功以後,將表單清空
				$('#regist_username').val('');
				$('#nickname').val('');
				$('#regist_password').val('');
				$('#final_password').val('');
				
			}else if(result.status == 4){
				/*到UserController再加一個UserNameException*/
				
				$('#regist_username').next().show().find('span').html(result.message);
			}else if(result.status == 3){
				$('#regist_password').next().show().find('span').html(result.message);
			}else{
				alert(result.message);
			}
		});
	//得到響應後,更新介面
	//**這個響應包刮正常&不正常
	
}
//cloudNote-day03-05 註冊功能4-----------------------尾

function checkPassword(){
	var password = $('#password').val();
	var rule = /^\w{4,10}$/;
	if( !rule.test(password)){
		$('#password').next().html('4~10個字符');
		return false;
	}
	$('#password').next().empty();
	return true;
}

//ㄅ3
function checkName(){
	console.log("checkName()");
	var name = $('#count').val();
	var rule=/^\w{4,10}$/;
	if( !rule.test(name)){
		$('#count').next().html('4~10個字符');
		return false;
	}
	$('#count').next().empty();
	return true;
}

function loginAction(){
	console.log("loginAction");
/*	
	//獲取用戶輸入的用戶名和密碼
				//id選擇器
	var name = $('#count').val();
	var password = $('#password').val();
	
	//使用json語法創建對象.
	//創建對象最重要的一件事:
	//data對象中的屬性名(name和password這兩個key) 要與 
	//控制器的參數名( login(name,password) )一致
	//不對應就傳不過去了.如:"name"寫成"nam",Server就收不到name.那name的默認值就是null
	var data = {"name":name,"password":password};
	
	//發送到Server端
	//$.ajax({});
	$.ajax({
		url:'user/login.do',
		data:data,
		//type是發送的方式
		type:'post',
		//此處可以寫get也可以寫post.只是按英文本意,所以寫post
		//記住!!!!凡是傳數據給Server,都用post
		//dataType是返回值
		dataType:'json',
		success:function(result){
			console.log(result);
		},
		//Q:result對象哪來的?
		//Ans: Server送回來的.
		//Q:這個result就是哪個對象?
		//Ans: JsonResult. JsonResult裡有3個屬性. 所以這個result也是3個屬性
		//務必記住!!!這個東西是從Server傳回來的
		error:function(e){
			alert("通信失敗");
		}
	});
*/
//上面有幾個問題要處理,所以:
	//1.帳密發送前應該要檢查一下,因為當帳密為空時,Server會報錯
	//Q:如何檢驗?
	//Ans:正則是最方便的
 	var name = $('#count').val();     
	var password = $('#password').val();
	var data = {"name":name,"password":password};
/* ㄅ2	
	//javascript的正則要在//之間寫
	var rule=/^\w{4,10}$/;
	//規定:從頭到尾(^$)單詞字符最少4個,最多10個
	if( !rule.test(name)){
		//java中,檢驗正則的方法是:String.match; javascript中則反過來了,是 正則.test
		//***若不確定,可以隨便打開一個網頁. 按F12,在console直接敲,如:
		//var rule = /^\w{4,10}$/;
		//rule.test('demo')
		
		//不滿足規則,則在span中予提示訊息
		//$('#count')已經選到區域了
					//透過上下文
		$('#count').next().html('4~10個字符');
		return;
	}
	//問題:不符合規則,提示有出現.但是,修改,符合規則後,提示卻沒消失
	//另外,現在要改成失去焦點時也檢驗.=>重構=>ㄅ
*/	
/***	
	//ㄅ4
	if( !checkName()){
		return;
	}
	
	if( !checkPassword()){
		return;
	}
	//但這麼調用有個問題:若不填帳密,直接點"登陸"
	//它只檢查用戶名. 密碼跳過去了
	//因為上方不滿足時,就已經return了.
	//	=>應該要checkName()和checkPassword()都執行完了,再return
	//某同學:若寫成如下呢?
	//if( !(checkName() || checkPassword())){}
	//師:狀況沒解決.因為一樣是短路的. 前面不滿足,後面也還是不執行
	//某同學:只寫一個槓?
	//師:那意思是  二進制的與運算
	//Q:怎辦?
	//Ans:使用javascript的特殊現象
***/
	var n = checkName()+checkPassword();
	if(n!=2){return;}
/****
 * 寫成乘法也行:
 * var success = checkName()*checkPassword();
 * if(!success){return;}
 */	
	$.ajax({
		url:'user/login.do',
		data:data,
		type:'post',
		dataType:'json',
		success:function(result){
			//2.Server送回來的結果還沒有作響應
			console.log(result);
			if(result.status==0){
				//登陸成功
				var user = result.data;
				console.log(user);
			//cloudNote-day04-03 筆記本列表5-----------始
			//登錄成功後,將userId保存到cookie中
			addCookie("userId",user.id);
			//上行user.id中的user指的是上幾行的var user
			//cloudNote-day04-03 筆記本列表5-----------末
				//跳轉到edit.html
				location.href='edit.html';
				//小技巧:
				//一切都測試完了,再跳.
				//不然,一跳轉,Browser刷新了,console那些狀態就看不見了.
				//這樣就不好調適.
			}else{
				var msg = result.message;
				//cloudNote-day03-01 登陸功能12
				//$('#count').next().html(msg);
				if(result.status == 2){
					$('#count').next().html(msg);
				}else if(result.status == 3){
					$('#password').next().html(msg);
				}else{
					alert(msg);
				}
				
			}
		},
		error:function(e){
			alert("通信失敗!");
		}
	});
	
}
