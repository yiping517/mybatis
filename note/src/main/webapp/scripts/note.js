//下面這兩變量是用來表示Server的響應結果
var SUCCESS = 0;
var ERROR = 1;

$(function(){
	
	var userId = getCookie('userId');
	console.log(userId);
	//上面兩行並不是正式編碼,
	//只是用來測試,登入成功後,能否在edit.html裡
	//獲得保存到cookie中的userId
	
	//意味著,網頁加載後,立即讀取筆記本列表.
	loadNotebooks();
	//某同學:為何不直接在這兒寫function內容呢?如:
	// $.ajax(...);
	//師:它一寫就一堆.若放此,往後將難以查閱.
	//另外,除此function,還會有其他function
	//建議:所以方法不要超過一個屏幕!!!!!!!!!!!
	
	/**
	 * on() 方法 綁定事件可以區分事件源
	 * click()方法 檔定事件,無法區分事件源
	 */
	//綁定筆記本列表區域的點即事件
	//$('#notebook-list').on("click",function(){
	//	console.log('click.無第二個參數:沒區別事件源');
	//});
	//某同學:為什麼不用click? 如下:
	// $('#notebook-list').click(function(){});?
	//師:現在的on和click運行結果完全一樣.
	//區別是:click無法區分事件源. 而on函數提供第三個參數,可以幫我們區分.
	
	//$('#notebook-list').on('click','li',function(){
		//意思是:只在li有效.
		//li指的就是筆記本列表項目.
		//若覺得li語意不清楚,還可以如此修改"
		//1.回到 var notebookTemplate處
		//2.在<li class="online 後面再加 notebook
		//Q:這是什麼意思?
		//Ans:class上空格後,又寫另一個class名. 這叫綁定2個class名.---ㄍㄍㄍ
		//ㄐㄐㄐ
		//如此,就可用用 .online選擇,也可以用 .notebook 選擇
		//加上notebook以後,代碼可以改成如下:
	/*
	$('#notebook-list').on('click','.notebook',function(){
		//這麼寫比較易讀:
		//筆記本列表在點擊筆記本時,執行一個方法.
		
		console.log("click事件:有綁定事件源");
	*/
	//替換成真正要執行的函數
	$('#notebook-list').on('click','.notebook',loadNotes);
	//Q:至此,解決什麼問題?
	//Ans:筆記本列表的點擊問題
	
	//cloudNote-day05-03 新建筆記功能1
	$('#note-list').on('click','#add_note',showAddNoteDialog);
	//=>在note-list這個區域上,點擊id為add_note的按鈕,就執行showAddNoteDialog方法
	//仍利用冒泡進行監聽
	
	//利用冒泡監聽"創建"按鈕
	$('#can').on('click','.create-note',addNote);
	//監聽整個id為can的區域. 點擊class為create_note的按鈕時,觸發addNote事件
	//跳出框的x和取消鈕也需要監聽
	//也透過冒泡進行監聽
	//仔細看網頁,就能找到class為cancel 及  class為close的
						//選擇器. 選擇器中有個主選擇器
	$('#can').on('click','.close,.cancel',closeDialog);
						//Q:上面這個選擇器是什麼意思?
						//Ans: class是close可以; class是cancel也可以. ,表示或者
						// ,選擇器叫選擇器組
						//如此,就用1個監聽,監聽了2件事.
	
});
function closeDialog(){
	console.log("closeDialog");
	$('.opacity_bg').hide();
	$('#can').empty();
}
function addNote(){
	console.log("addNote");
	/*
	$.ajax({
		url:'/note/add.do',
		data:
	});
	*/
}
function showAddNoteDialog(){
	$('#can').load('alert/alert_note.html',
		function(){//網頁加載後後執行的
			//為了讓對話框好看一點的小技巧: 襯底
			//在id為can的div前可以發現一個class是opacity_bg的div. 它是半透明背景
			$('.opacity_bg').show();
			//因為這個class為opacity_bg的半透明背景目前是隱藏的,
			//網頁加載完要把它顯示出來
			//發布後側試,可以發現對話框後面是個半灰色的背景,就是剛剛class為opacity_bg的div
			//Q:這背景我們怎設計的?
			//Ans:對話框彈出來才顯示.
			
			//cloudNote-day05-03 新建筆記功能1
			//對話框顯示完了以後,控件獲得焦點
			//=>找到控件 & 取得焦點
			$('#input_note').focus();
			//注意!!!!上行務必寫到第二個參數的function裡
			
	});
}
/**
function showAddNoteDialog(){
	$('.opacity_bg').show();
	//若將上行寫在此,意思是:先顯示背景
	$('#can').load('alert/alert_note.html',
		function(){
	});
}
*/

//筆記本項目點擊事件處理方法,加載全部筆記
function loadNotes(){
	
	//調用Controller
	//$.getJSON(url,data,function(result){});
	//湊齊所需要的3個參數:
	//1. url非常好解決
	var url = "note/list.do";
	
	//2.問題就出在data上.
	//Server要接收notebookId這個參數.
	//但咱現在沒有. 
	//分析一下它哪來:
	//1)首先,這個區塊沒有notebookId
	//2)Q:什麼時候有過?
	//Ans:上一個方法---showNotebooks(notebooks)裡有筆記本的id.
	//這個方法裡面,在顯示時,是有筆記本的.
	//且每個筆記本都有兩個屬性:notebook.id, notebook.name
	//其中,name屬性已經顯示出來了. 但id被我們礽了.
	//所以,要把id存起來.
	//Q:存在哪?
	//Ans:每一個li,即每一個筆記本都對應一個id. 現在,咱就把它存到li上
	//Q:什麼意思?
	//Ans: jQuery有一個方法,可以幫我們把數據綁到li上
	//Q:這個方法怎麼綁定呢?
	//Ans:它的作法就是:
	//			li.data('notebookId',id);
	//創建li時,就調用這個方法.把id綁到li上.((因為筆記本和id是一對一的))
	//如此,在點擊事件裡面,拿到li,再調用data()取到id
	//這個data()有2種模式:
	//1))給它2個參數:('DOM元素',?)_ 一個是被綁定的元素,另一個是要綁到元素的值
	//2))一個參數時_就是把值取回來
	
	//另一個問題:如何獲取li?
	//Ans: $(this)
	//Q:為什麼?
	//Ans:因為點擊哪個對像,哪個對象就是this
	
	//所以,接下來要重構showNotebooks()----->ㄊㄊㄊ
//ㄖㄖㄖ-------------------始
	var li = $(this);//當前被點擊的對象
	
//ㄙㄙㄙ
	//Ans:找到li的父元素(ul),然後
	li.parent().find('a').removeClass('checked');
	
	//===>處理滑鼠移開後,就無法區分選了哪個筆記本的問題:
	//Q:怎麼選定呢?
	//Ans:在li的a上加個checked,增加選定效果.
	//找到li裡的a元素,然後給它加上一個類
	li.find('a').addClass('checked');
	//但現在還是有個問題:若選擇別的li,原本被選的那個li的選定效果並沒有去掉.----ㄙㄙㄙ
	
	
	var data={notebookId:li.data('notebookId')};
	console.log("-------------"+data);
	$.getJSON(url,data,function(result){
		console.log("-----------------result-----------------");
		console.log(result);
		if(result.status==SUCCESS){
			console.log("進到$.getJSOM裡的success");
			var notes = result.data;
			showNotes(notes);
		}else{
			alert(result.message);
		}
	});
}

//將筆記列表信息顯示到屏幕上
function showNotes(notes){
	console.log("showNotes");
	console.log(notes);
	//將每個筆記對象顯示到屏幕的ul區域
	//先找到顯示區域
	var ul = $('#note-list ul');
	ul.empty();
	
	for(var i=0; i<notes.length; i++){
		//找到每一個筆記
		var note = notes[i];
		console.log("notes.length="+notes.length);
		//替換
		var li = noteTemplate.replace('[title]',note.title);
		/***另一種方法:
		 * 利用jQuery埃個兒創建DOM元素,挨個兒加屬性
		 */
		console.log("li="+li);
		li=$(li);
		ul.append(li);
	}
	
}

var noteTemplate = '<li class="online">'+
			'<a>'+
				'<i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i> [title]<button type="button" class="btn btn-default btn-xs btn_position btn_slide_down"><i class="fa fa-chevron-down"></i></button>'+
			'</a>'+
			'<div class="note_menu" tabindex="-1">'+
				'<dl>'+
					'<dt><button type="button" class="btn btn-default btn-xs btn_move" title="移动至..."><i class="fa fa-random"></i></button></dt>'+
					'<dt><button type="button" class="btn btn-default btn-xs btn_share" title="分享"><i class="fa fa-sitemap"></i></button></dt>'+
					'<dt><button type="button" class="btn btn-default btn-xs btn_delete" title="删除"><i class="fa fa-times"></i></button></dt>'+
				'</dl>'+
			'</div>'+
		'</li>';

function loadNotebooks(){
	//調用ajax
	//利用ajax從Server獲取(get)數據
	//$.getJSON();
	//$.getJSON(url,data,function(result){
	//**$.getJSON和$.post 參數   作用一樣
	//這兩個方法最大的差別:業務目的不同.
	//**其底層網路留存((?))略有差別,但能相互替代.
	//建議:按照業務使用.
	//});
	
	//先湊齊需要的3個參數:
	var url="notebook/list.do";
	//var data = {userId:getCookie('userId'),name:demo}
	var data = {userId:getCookie('userId')};
	//上行程式碼是先執行getCookie.
	//把getCookie的結果拿到,放到 : 後面
	//然後整體創建的一個javascript對象.
	//這個對象中有個屬性userId;值是getCookie的結果.
	//一定要搞清楚它的運算順序!!!!!!!!!!!!!!!
	//然後,等號右邊創建的是一個json對象.就是一個javascript對象.
	//然後,在下面發送時,並不是把javascript對象發送到Server端
	//而是把那個屬性的值發到Server端.
	//它沒有把對象發過去. 它發的是對象中的數據.
	//Q:在哪能看見呢? 
	//Ans: F12裡看通訊.就能看見,它真正網絡發送時,
	//並沒有發送那個對象,而把對象中的數據發過去了.
	//就是,發的時候變形了.不是這個東西了.
	$.getJSON(url,data,function(result){
		console.log(result);
		//若能在Browser console看到,說明了獲取Server送回來的對象數據
		//data屬性是個數組. 數組裡有我們要的數據.
		//但這些數據在Server端不是數組,也不是對象,而是List<Map>
		//數組中的每個Object在Server端是Map. 裡面有id和name
		
		//"F12"->"network"->"xhr"
		//而在這個異步通訊的代碼中,
		//因為是get請求,所以請求參數在url上.
		//這個url上的數據就是data中的數據.
		//$.getJSON方法幫我們把第二個參數data裡的數據,放到了url上.發到了Server端
		//所以,客戶端 jQuery並沒有把對象發到Server上. 而把對象拆了.
		//拆散了. 把裡面的數據放到url上發送
		//而且,這個url叫queryString.
		//也就是下面的那個"Query String Parameters"
		//咱可以驗證一下: 在data裡再寫一組數據. 如此url上也會多一組數據.
		//jQuery將json對象,也就是那個data對象,轉換為get的參數,發送到Server端
		//並沒有直接把json對象發到Server端.
		
		//展示從Server上拿到的數據.
		//在edit.html能找到<h3>全部筆記本. 它下面有個<ul>. <ul>下有個<li>
		//因此,咱只需要將Server送回來的筆記本列表中的每一項,轉換成對應的li,加到頁面中.
		//如此,頁面中就能把所有的筆記本列出來
		if(result.status == SUCCESS){
			var notebooks = result.data;
			console.log(notebooks);
			//在showNotebooks方法中,將全部的筆記本數據notebooks顯示到notebook-list區域
			showNotebooks(notebooks);
		}else{
			alert(result.message);
		}
	});
}

//在edit.html id為notebook-list區域中,顯示筆記本列表
function showNotebooks(notebooks){
	
	//下面兩個註解就是showNotebooks的算法
	//(1)找到顯示筆記本列表的區域
	//此處利用派生選擇器	
	//Q:什麼是"派生選擇器"?
	//Ans:找某一元素中的子子孫孫的某一元素
	var ul = $('#notebook-list ul');
	
	//(2)遍歷notebooks數組,為每個對象創建一個li元素,添加到ul元素中.
/**
	for(var i=0; i<notebooks.length; i++){
		//var寫不寫都行.因為javascript是弱類型語言
		
		var notebook = notebooks[i];
		//找到每個元素後,就為每個元素創建li
		//var li = $('<li></li>');
		//jQuery的$函數不僅有在網頁中查詢一個DOM元素的功能,
		//還有創建一個DOM元素的功能.
		//Q:如何用jQuery的$函數創建一個DOM函數呢?
		//Ans:只要把一個滿足DOM規則的字符串給它
		
		//替剛剛創建的li元素添內容
		var li = $('<li></li>').html(notebook.name);
		//這個name是從Server端傳過來的.
		//調用html方法後,這個方法的傳回值仍是li對象
		//jQuery對象的方法有個特點:幾乎每個方法返回的都是當前對象
		
		//將剛剛創建的li加到ul
		ul.append(li);
	}
*/
	for(var i=0; i<notebooks.length; i++){
		var notebook = notebooks[i];
		var li = notebookTemplate.replace('[name]',notebook.name);
		//生成li
		li = $(li); 
		//上行不寫,也能append.
		//因為append方法既能接收jQuery對象,也可以接收字符串.
		
//ㄊㄊㄊ-----------------------------始
		//將notebook.id綁定到li
		//('id',value)
		li.data('notebookId',notebook.id);
		//它既然有id,就意味它可以綁很多.
		//綁好以後,回到loadNotes()----->ㄖㄖㄖ
//ㄊㄊㄊ-----------------------------尾		
		ul.append(li);
	}
}
var notebookTemplate = 
//ㄍㄍㄍ
//	'<li class="online">'+
	'<li class="online notebook">'+ //回到ㄐㄐㄐ
	'<a><i class="fa fa-book" title="online" rel="tooltip-bottom"></i> [name]</a>'+
	+'</li>';
//**這個<i></i>是字體圖標. 用css技術做的. 叫css字體圖標
//上面的方式是:弄個字符串,透過replace函數做替換.
/**也可以使用另一種方法:
 * 在[name]那定一個span元素
 * 然後對那個span元素內容進行替換
*/
/*另一種方式:字符串連接*/











