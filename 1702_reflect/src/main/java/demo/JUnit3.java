package demo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

/**
 * JUnit3原型
 * 	原型不是全功能. 試出來就行.
 * 
 * ****jUnit4多的註解也是用反射解析的
 */
public class JUnit3 {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		//step1.動態加載類
		//step2.動態創鍵對象
		//step3.找到類中全部的方法
		//step4.遍歷查找哪些方法以test為開頭
		//step5.執行這些方法
		
		//step1.動態加載類
		Scanner in = new Scanner(System.in);
		System.out.println("輸入類名");
		String className = in.nextLine();
		Class cls = Class.forName(className);
		
		//step2.動態創鍵對象
		Object obj = cls.newInstance();
		
		//查找方法
		Method[] methods = cls.getDeclaredMethods();
		
		for(Method method : methods) {
			//檢查方法名是否以test開頭
			String name = method.getName();
			//
			if(name.startsWith("test")) {
//ㄅㄅㄅ				
method.setAccessible(true);			
//但這件事本身是在破壞封裝.
//私有屬性亦可藉此調用

				//動態調用方法
				method.invoke(obj);
				//剛剛定義的方法都沒有返回值,那就不用接收返回值
				//且若沒有返回值,還接收返回值,
				//那返回值將會是空, null
			}
		}
	}
	/*運行後的輸出結果:
			輸入類名
			demo.TestCase
			testHello()
			ABC
	 */
	
	/*
	 在所欲動態加載進來的類裡加一個私有方法後的輸出結果:
			輸入類名
			demo.TestCase
			testHello()
			Exception in thread "main" java.lang.IllegalAccessException: Class demo.JUnit3 can not access a member of class demo.TestCase with modifiers "private"
				at sun.reflect.Reflection.ensureMemberAccess(Reflection.java:102)
				at java.lang.reflect.AccessibleObject.slowCheckMemberAccess(AccessibleObject.java:296)
				at java.lang.reflect.AccessibleObject.checkAccess(AccessibleObject.java:288)
				at java.lang.reflect.Method.invoke(Method.java:491)
				at demo.JUnit3.main(JUnit3.java:39)
	 */
	//上面的解決辦法----->多加ㄅㄅㄅ那一行
	
}
