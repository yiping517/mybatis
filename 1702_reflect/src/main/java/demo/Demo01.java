package demo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

/**
 * 測試 反射API
 * 
 * 這段程序最大的特點:
 * 	編譯完,運行期間加載哪個類並不知道.
 * 	運行時才知道.
 */
public class Demo01 {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		//-----動態加載類----
		Scanner in = new Scanner(System.in);
		//即使沒有Scanner,寫死的,它仍是動態加載
		//我們只是使用Scanner體現類名是動態獲取的.
		
		//下面的動態加載不僅能加載咱所創建的類
		//也能動態加載系統API或任何一個類
		//如: java.util.Date
		//或從他處導了一個jar包
		//如: jdbc的驅動. 用它加載驅動也行.
		System.out.println("輸入類名");
		//一定要輸入類的全名
		//所以,剛剛創建的是: demo.Foo
		String className = in.nextLine();
		//---1.調用方法
		Class cls = Class.forName(className);
		System.out.println(cls);
		//輸出加載以後的類名
		System.out.println(cls.getName());
		
		//cloudnote2-day02-11 動態創鍵對象
		//Q:為什麼此處叫動態創鍵對象?
		//Ans:因為類名根本不知道.所以這個程序運行期才知道創建哪個對象.
		//這就是動態創鍵對象的特點.
		Object obj = cls.newInstance();
		//此時,仍有編譯錯誤.
		//Q:這是因為異常. 什麼時候出這異常?
		//Ans:找不到無參構造器時.=>InstantiationException  IllegalAccessException
		System.out.println(obj);
		
		//在下面console輸入demo.Foo後的輸出:
		/**
		 class demo.Foo =>表示動態加載類了
		 demo.Foo =>類名
		 demo.Foo@1b6d3586 
		 	=>表示動態創建對象.
		 	java默認的toString()的輸出結果
		 */
		
		//cloudnote2-day02-12 反射可以查詢類中的方法
		//-----動態檢查類中聲明的方法信息-----
		Method[] methods = cls.getDeclaredMethods();
		for(Method m : methods) {
			System.out.println(m);
			//在下面console輸入demo.Foo後的輸出結果:
			/**
			  class demo.Foo
			  demo.Foo
			  demo.Foo@1b6d3586
			  public int demo.Foo.demo()
			  public java.lang.String demo.Foo.hello()
			 */
						//返回值			//方法名		
			//無論是返回值還是方法名,它用的都是全名.
			//底層工作把包名也補上了.
			//平時咱寫hello,但它底層工作時,把包名點類名都補全了.
			
			//由輸出結果可以知道,類中有2個方法.
			
		}

		//cloudnote2-day02-13 動態執行方法
		//動態找到一個方法
		System.out.println("輸入方法名");
		String name = in.nextLine();
		Method m = cls.getDeclaredMethod(name);
		//它會有個異常:方法名給錯了
		
		//動態執行方法
		//val是方法執行後的返回值
		Object val = m.invoke(obj);
		//此處會有個異常. 因為方法本身執行時,也可能會有異常.
		//因為在此處,咱並不知道是哪個方法.
		//不知道哪個方法,那個方法有沒有異常,咱也不知道.
		//而且即便咱寫的都正確,方法本身仍可能拋出NullPointerException
		//還有另一種情況: m和Object不配套.
		//前面說了,m和Object必須配套.
		
		System.out.println(val);
		/**輸出結果:
				輸入類名
				demo.Foo
				class demo.Foo
				demo.Foo
				demo.Foo@1b6d3586
				public int demo.Foo.demo()
				public java.lang.String demo.Foo.hello()
				輸入方法名
				hello
				hello_11/8_cloudnote2-day02-11 動態創鍵對象
		 */
		
		/*
		 * 當方法名輸錯時的錯誤訊息:
				輸入類名
				demo.Foo
				class demo.Foo
				demo.Foo
				demo.Foo@1b6d3586
				public java.lang.String demo.Foo.hello()
				public int demo.Foo.demo()
				輸入方法名
				hell
				Exception in thread "main" java.lang.NoSuchMethodException: demo.Foo.hell()
					at java.lang.Class.getDeclaredMethod(Class.java:2130)
					at demo.Demo01.main(Demo01.java:81)
		 */
	}
}
