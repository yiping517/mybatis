package demo;

public class Foo {

	// cloudnote2-day02-11 動態創鍵對象
	//Q:這個類有無參構造器嗎?
	//Ans:有. 默認構造器.
	
	public String hello() {
		return "hello_11/8_cloudnote2-day02-11 動態創鍵對象";
	}
	
	public int demo() {
		return 118;
	}
}
