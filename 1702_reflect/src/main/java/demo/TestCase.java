package demo;
//之前老師教的測試案例是jUnit4. 用註解寫的.
//這是jUnit3的原型.
//jUnit3不是以註解為開頭,而是用方法名. 
//方法名用test,它就會自動執行.
//而jUnit4進化了.用註解.用註解以後,方法名叫啥都行.
//從上說來,jUnit就是用反射寫的
//**mybatis, spring等這些框架也都是用反射做的
public class TestCase {

	public void testHello() {
		System.out.println("testHello()");
	}
	
	public void testABC() {
		System.out.println("ABC");
	}
	
	private void testPrivate() {
		System.out.println("testPrivate()");
	}
}
