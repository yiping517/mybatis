package test;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import dao.EmployeeDAO;
import entity.Employee;


public class TestCase {

	private SqlSession session;
	
	//負責獲得SqlSession對象
	@Before
	public void init() {
		//獲得SqlSession對象有3小步:
		//step1.先創建SqlSessionFactoryBuilder對象
		String config = "SqlMapConfig.xml";
		SqlSessionFactoryBuilder ssfb = new SqlSessionFactoryBuilder();
		
		//step2.通過SqlSessionFactoryBuilder對象創建SqlSessionFactory
		SqlSessionFactory ssf = ssfb.build(TestCase.class.getClassLoader().getResourceAsStream(config));
		//參數需要傳入一個輸入流,讀取SqlMapConfig.xml的內容
		
		//step3.通過SqlSessionFactory對象獲得SqlSession對象
		session = ssf.openSession();
	}
	
	//測試EmployeeDAO
	@Test
	public void test1() {
		//獲得EmployeeDAO的實現
		//透過SqlSession的getMapper()
		EmployeeDAO dao = session.getMapper(EmployeeDAO.class);
		//這個方法的涵義:讓myBatis生成一個符合接口要求的對象
		//所以會返回一個符合Mapper映射器要求的對象
		
		//創建Employee對象
		Employee e = new Employee();
		e.setName("test109");
		e.setAge(76);
		
		//既然對象有了,就可以調用它的方法了
		dao.save(e);
		
		//涉及數據的刪除添加修改,還得提交事務
		session.commit();
		
		//關閉
		session.close();
	}
	//映射文件的命名空間未與DAO interface一致時報的錯誤:
	//org.apache.ibatis.binding.BindingException: 
	//Type interface dao.EmployeeDAO is not known to the MapperRegistry.

	//查出所有員工
	@Test
	public void test2() {
		//先獲得Mapper映射器的實現
		EmployeeDAO dao = session.getMapper(EmployeeDAO.class);
		//將接口對應的對象作為參數傳入
		//**之前學jdbc時,寫過類似代碼:
		//連接(Connection)是個接口.
		//Connection conn = DriverManager.getConnection()
		//DriverManager.getConnection()會返回一個符合Connection接口要求的對象
		//這個所返回的對象是由jdbc驅動所負責生成的
		//此處類似.
		
		//調用方法
		List<Employee> result = dao.findAll();
		System.out.println(result);
		
		//關閉
		session.close();
	}
	
	
	//測試 根據員工id查詢
	@Test
	public void test3() {
		EmployeeDAO dao = session.getMapper(EmployeeDAO.class);
		Employee e = dao.findById(3);
		//Employee e = dao.findById(33);//null
		System.out.println(e);
		session.close();
	}
	
	//測試modify
	@Test
	public void test4(){
		EmployeeDAO dao = session.getMapper(EmployeeDAO.class);
		Employee e = dao.findById(1);
		System.out.println(e);
		//輸出:Employee [id=1, name=學myBatis, age=107]
		e.setName("1010修改");;
		dao.modify(e);
		session.commit();
		System.out.println(e);
		//Employee [id=1, name=1010修改, age=107]
		session.close();
	}
	
	//測試delete
	@Test
	public void test5() {
		
	}
	
	//測試 findById2
	@Test
	public void test6() {
		
	}
	
	//測試 findById3
	@Test
	public void test7() {
		
	}
}
