package dao;

import java.util.List;
import java.util.Map;

import entity.Emp;
import entity.Employee;

/**
 * 這個接口在myBatis中叫做Mapper映射器.
 * Q:什麼是Mapper映射器?
 * Ans:符合映射文件要求的接口.
 * Q:有哪些要求?
 * Ans:
 * 1)方法名要與sql id一致
 * 2)方法的參數類型要與parameterType一致
 * 3)方法的返回類型要與resultType一致
 * 4)映射文件的namespace要等於接口的完整名字
 *		=>修改映射文件(EmpMapper.xml)的namespace
 */
public interface EmployeeDAO {

	//接口中,方法的名字要與sql id一樣.
	//現在要把一個員工插入到數據庫,所以選的是 id="save"的那一個sql
	//所以方法名是save
	//Q:那參數類型呢?
	//Ans:根據映射文件id = save的那條sql的parameterType可知,參數類型是Employee
	public void save(Employee e);
	//此時,寫完這個接口以後,就可以用了.
	//因為Mybatis會幫我們生成一個符合接口要求的對象
	
	
	//查詢方法
	//public Employee findAll(); --->我寫的
	//返回值應該是個list. 
	//根據映射文件,list裡面是Employee對象
	public List<Employee> findAll();
	
	
	//根據員工id查尋
	public Employee findById(int id);
	
	public void modify(Employee e);
	
	public void delete(int id);
	
//	public Map<Employee> findById2(int id);
	public Map findById2(int id);
	
	public Emp findById4(int id);
	
}
