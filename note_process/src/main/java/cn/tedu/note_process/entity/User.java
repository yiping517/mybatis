package cn.tedu.note_process.entity;

import java.io.Serializable;

//要符合java bean語法,所以要實現序列化接口
public class User implements Serializable {
	//序列化版本號的值無所謂. 是個數就行. 幾都行. 重點是穩定就行.
	//它的目的就是保證對象的序列化是穩定的
	private static final long serialVersionUID = -6832075902707123999L;
	
	private String id;
	private String name;
	private String password;
	private String token;
	private String nick;
	
	public User() {
	}

	public User(String id, String name, String password, String token, String nick) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.token = token;
		this.nick = nick;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", token=" + token + ", nick=" + nick
				+ "]";
	}
	//因為有id,所以要寫equal hashcode
	//凡是有id的實體類,要重寫equal hashcode
	//用工具生成時,一定要注意:屬性選的是id
	//比較關鍵屬性兩個是否相等. 
	//即,兩個對像id一樣,就是一樣的.
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
