package cn.tedu.note_process.service;

public class PasswordException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3507671373543312991L;

	public PasswordException() {
		// TODO Auto-generated constructor stub
	}

	public PasswordException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PasswordException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PasswordException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PasswordException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
