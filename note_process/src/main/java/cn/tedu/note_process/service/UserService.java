package cn.tedu.note_process.service;

import cn.tedu.note_process.entity.User;

/**
 * 業務層接口
 *
 */
public interface UserService {
	/**
	 * 登入功能.
	 * 成功,返回用戶信息;失敗,拋異常.
	 * @param name
	 * @param password
	 * @return 用戶信息
	 * @throws UserNotFoundException:用戶不存在
	 * @throws PasswordException:密碼錯誤
	 */
	User login(String name,String password) throws UserNotFoundException, PasswordException;
	//此時有編譯錯誤
	
	//創建異常類.
	//一般情況下:寫一個類,拿異常當父類繼承一下就行了.
	//但,那麼寫慢. 有個簡單的辦法: 步驟
	//1.點編譯錯誤那行數字處的X一下
	//2.選"Create class'UserNotFoundException'"
	//3.將superclass處的"java.lang.Exception"改成"java.lang.RuntimeException"
	//4.在"Which method stubs would you like to create?"的選項處,勾上"Constructors from superclass"
}
