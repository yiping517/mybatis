package cn.tedu.note_process.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.tedu.note_process.dao.UserDao;
import cn.tedu.note_process.entity.User;
import cn.tedu.note_process.service.PasswordException;
import cn.tedu.note_process.service.UserNotFoundException;
import cn.tedu.note_process.service.UserService;

@Service("userService4")
public class UserServiceImpl4 implements UserService {
	
	@Resource
	private UserDao userDao;
	
	public User login(String name, String password) throws UserNotFoundException, PasswordException {
		//提防前面說的空指針異常的辦法:
		//1.確保他一定引用對象.
		//如上面的userDao. 咱加了@Resource以後,就可以確保它一定引用UserDao對象
		//咱在運行期間調用它的屬性方法,就不用擔心它空指針異常
		//因為@Resource一定會注入的
		//2.而password咱無法控制怎麼注入.所以,判斷一下:
//		if(password==null) {
//			throw new PasswordException("密碼空");
//		}
		
		//另一個需要考慮的情況:去空白
		if(password==null||password.trim().isEmpty()) {
			throw new PasswordException("密碼空");
		}
		//Q:或前後能交換順序嗎?
		//Ans:不行.若交換,就存在空指針異常的風險了.
		//因為,當password薇空的時候,若直接調trim()
		//當引用類型變量是空,訪問其屬性/方法就會出問題:空指針異常
		//而恰恰短路的或運算. 
		//第一個表達式滿足,即password為空時,
		//第二個表達式就不計算,即跳過空指針異常的風險了.
		//所以此處,或運算兩端絕對不能交換順序.
		if(name==null || name.trim().isEmpty()) {
			throw new UserNotFoundException("用戶名為空");
		}
		
		User user = userDao.findUserByName(name.trim());
		if(user==null) {
			throw new UserNotFoundException("name錯誤");
		}
		if(password.equals(user.getPassword())) {
			return user;
		}
		throw new PasswordException("密碼錯誤");
	}
}
