package cn.tedu.note_process.service.impl;

import org.springframework.stereotype.Service;

import cn.tedu.note_process.entity.User;
import cn.tedu.note_process.service.PasswordException;
import cn.tedu.note_process.service.UserNotFoundException;
import cn.tedu.note_process.service.UserService;

@Service("userService2")
//而且,我們會給它一個id
//一般會按照接口名去命名實現類的id.
//寫到這要想:這個@Service現在有沒有被組件掃描到?
//Ans:沒有.因為此前的配置文件並沒有掃描cn.tedu.note_process.service這個包

//為了讓這個@Service可以工作,咱得再加一個配置,讓他對這個包進行組件掃描.
//所有的註解都是跟著一個配置對應的.那個配置不寫,對應的註解就無法生效.
public class UserServiceImpl2 implements UserService {
	
	public User login(String name, String password) throws UserNotFoundException, PasswordException {
		User user = userDao.findUserByName(name);
		//上行的userDao出現編譯錯誤
		//因為現在沒有userDao對象
		//Q:怎辦?
		//Ans:咱寫軟件時,這兒用了一個變量.
		//這個變量一定對應一個對象or這個變量所對應的對象...
		//所有的變量所對應的對象有3種來源:
		//1.自己new一個
		//2.從工廠獲得一個
		//3.注入一個((別人給的))
		if(user==null) {
			throw new UserNotFoundException("name錯誤");
		}
		return null;
	}

}
