package cn.tedu.note_process.service.impl;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import cn.tedu.note_process.dao.UserDao;
import cn.tedu.note_process.entity.User;
import cn.tedu.note_process.service.PasswordException;
import cn.tedu.note_process.service.UserNotFoundException;
import cn.tedu.note_process.service.UserService;

@Service("userService5")
public class UserServiceImpl5 implements UserService {
	
	@Resource
	private UserDao userDao;
	
	public User login(String name, String password) throws UserNotFoundException, PasswordException {
		if(password==null||password.trim().isEmpty()) {
			throw new PasswordException("密碼空");
		}

		if(name==null || name.trim().isEmpty()) {
			throw new UserNotFoundException("用戶名為空");
		}
		
		User user = userDao.findUserByName(name.trim());
		if(user==null) {
			throw new UserNotFoundException("name錯誤");
		}
		
		String salt = "今天你吃了嗎?";
		String pwd = DigestUtils.md5Hex(salt+password.trim());
		if(pwd.equals(user.getPassword())) {
			return user;
		}
		throw new PasswordException("密碼錯誤");
	}
}
