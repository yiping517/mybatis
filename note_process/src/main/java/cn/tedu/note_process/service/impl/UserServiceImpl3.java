package cn.tedu.note_process.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.tedu.note_process.dao.UserDao;
import cn.tedu.note_process.entity.User;
import cn.tedu.note_process.service.PasswordException;
import cn.tedu.note_process.service.UserNotFoundException;
import cn.tedu.note_process.service.UserService;

@Service("userService3")
public class UserServiceImpl3 implements UserService {
	
	//咱只需要在程序中定義一個變量:
	//UserDao userDao;
	//但現在上面這個變量是空的.
	//一運行就空指針異常.解決:
	@Resource
	private UserDao userDao;
	//Q:UserDao到底誰創建的?
	//開啟spring-mybatis2.xml文件.
	//可見這麼一個bean: MapperScannerConfigurer
	//這個映射接口的掃描器會自動掃描cn.tedu.note_process.dao這個包
	//然後把dao包中的接口自動實例化成對象
	//Q:這些都由誰來管呢?
	//Ans:嚴格說,這不是spring提供的組件
	//而是mybatis提供的類. 但它利用的spring的機制
	
	public User login(String name, String password) throws UserNotFoundException, PasswordException {
		User user = userDao.findUserByName(name);
		if(user==null) {
			throw new UserNotFoundException("name錯誤");
		}
		//if(password==user.getPassword()) {}
		//上行錯誤.
		if(password.equals(user.getPassword())) {
			return user;
		}
		throw new PasswordException("密碼錯誤");
	}
//程序至此有bug: password若是null,將出現空指針異常
//技巧:任何時候,只要程序中有引用類型變量,如:user, password, name	
//就要想清楚它什麼時候賦的值.它賦的值中有沒有可能null的情況
//Q:什麼時候出空指針異常?
//Ans:當一個引用類型變量,其值為空的時候,
//我們訪問其屬性or方法,將出現空指針異常.	
//不許說:一個對象是空的時候 @@@@@@@@@@@@@@@@@	
//因為:既然已經說是對象了,對象就一定存在,不可能為空.
//Q:上面的user, password, name是對象?還是引用對象的變量?
//Ans:引用對象的變量.
//所以在java中,根本就不存在對象為空這件事.
//java中存在的是:引用類型變量的值是空的
}
