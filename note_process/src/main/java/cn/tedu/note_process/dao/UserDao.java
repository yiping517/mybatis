package cn.tedu.note_process.dao;

import cn.tedu.note_process.entity.User;

public interface UserDao {
	User findUserByName(String name);
	//編譯錯誤:缺一個User實體類
	//實體類一定要跟數據庫的表的設計有關
	//數據庫有幾個屬性,就設幾個屬性
	
	//寫完User實體類後. 此處就沒有編譯錯誤了.
	//此時的下一步就是:寫sql
}
