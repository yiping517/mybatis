package cn.tedu.note_process.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tedu.note_process.entity.User;
import cn.tedu.note_process.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Resource
	private UserService userService;
	
	@RequestMapping("/login.do")
	//因為要採用Ajax的工作方式. 所以搭配註解:
	@ResponseBody
	//控制器方法參數要跟瀏覽器提交的名字對應.
	public Object login(String name,String password) {
		User user = userService.login(name, password);
		return user;
	}
}
