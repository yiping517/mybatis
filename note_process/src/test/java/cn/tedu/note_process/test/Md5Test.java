package cn.tedu.note_process.test;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

public class Md5Test {

	@Test
	public void testMd5() {
		String str = "123456";
		String md5 = DigestUtils.md5Hex(str);
		System.out.println(md5);
		//但上述安全性比較差.
		//Q:那咱怎麼進行加鹽摘要呢?
		String salt = "今天你吃了嗎?";
		md5 = DigestUtils.md5Hex(salt+str);
		System.out.println(md5);
	}
}
