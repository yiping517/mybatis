package cn.tedu.note_process.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.tedu.note_process.entity.User;
import cn.tedu.note_process.service.UserService;

public class UserServiceTest {

	ClassPathXmlApplicationContext ctx;
	
	@Before
	public void initCtx() {
		ctx = new ClassPathXmlApplicationContext(
				"conf/spring-mvc.xml",
				"conf/spring-mybatis2.xml",
				"conf/spring-service.xml");
	}
	
	@After
	public void close() {
		ctx.close();
	}
	//寫至此. 老師:上面一樣的代碼已經重複寫多次了
	
	@Test
	public void testLogin() {
		String name = "demo";
		String password = "123456";
		UserService service = ctx.getBean("userService5", UserService.class);
		User user = service.login(name, password);
		System.out.println(user);
	}
	//從mySQL進行select語句查詢後,可以發現:
	//數據庫存密碼是加密的.
	//與之進行對比當然會不一樣
	
}
