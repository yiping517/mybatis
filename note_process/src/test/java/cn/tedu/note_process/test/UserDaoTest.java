package cn.tedu.note_process.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.tedu.note_process.dao.UserDao;
import cn.tedu.note_process.entity.User;

public class UserDaoTest {
	
	ClassPathXmlApplicationContext ctx;
	
	@Before
	public void initCtx() {
		ctx = new ClassPathXmlApplicationContext(
				"conf/spring-mvc.xml",
				"conf/spring-mybatis2.xml");
		//初始化容器時,是可以加載多個配置文件的
	}
	
	@After
	public void close() {
		ctx.close();
	}
	
	//以上都還只是搭了個環境
	//測試要用@Test
	@Test
	public void testFindUserByName() {
		String name="demo";
		UserDao dao = ctx.getBean("userDao", UserDao.class);
	//ctx.getBean能幫我們從spring容器獲取一個bean對象
	//Q:這個bean對象是spring創建的?還是mybatis-spring插件創建的?
	//Ans:後者裡面有個MapperScannerConfigurer創建的. 	
	//MapperScannerConfigurer掃描Dao的接口包時創建的
		User user = dao.findUserByName(name);
		System.out.println(user);
	}
}
