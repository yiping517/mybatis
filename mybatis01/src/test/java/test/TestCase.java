package test;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import entity.Employee;

public class TestCase {

	@Test
	public void test1() {
					//配置文件
		String config="SqlMapConfig.xml";
		
		//創建SqlSession對像,以執行sql語句
		//step1.創建SqlSessionFactoryBuilder對像
		SqlSessionFactoryBuilder ssfb = new SqlSessionFactoryBuilder();
		//step2.創建SqlSessionFactory對像
		//SqlSessionFactory ssf = ssfb.build(inputStream);
		//透過輸入流的方式讀取文件
		//Q:怎麼構建這個輸入流呢?
		//Ans:類加載器有個方法:getResourceAsStream(). 它可以構造一個輸入流. 如此,就能通過這個流,讀取文件內容了
		SqlSessionFactory ssf = ssfb.build(TestCase.class.getClassLoader().getResourceAsStream(config));
		//step3.獲得sqlSession對像
		SqlSession session = ssf.openSession();
		
		//ㄅㄅㄅ
		Employee e = new Employee();
		e.setName("學myBatis_Day2_給修改用");
		e.setAge(98);
		
		//調用SqlSession對象提供的方法訪問數據庫
		//與映射文件對應:
		//(inserr sql的id[最好也把命名空間加進來]=>要執行的sql的id , 員工對像[=>與映射文件中的parameterType對應])
		//  =>要把員工對像插入到數據庫
		//所以,先創立一個員工對像---ㄅㄅㄅ
		//**之前說過,命名空間是用來區分同名的元素
		session.insert("test.save", e);
		
		//插入操作(添加,修改,刪除)需要提交事務
		session.commit();
		//myBatis的底層仍是jdbc. 它把自動提交改成手動提交
		
		//關閉SqlSession
		session.close();
	}
}
