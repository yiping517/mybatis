package test;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.EmployeeDAO;
import entity.Employee;

public class TestCase {

	@Test
	public void test1() {
		//啟動spring容器
		String config = "mybatis-spring.xml";
		ApplicationContext ac = new ClassPathXmlApplicationContext(config);
		/**
		 * 執行後,出現錯誤訊息:
		 * org.springframework.beans.factory.xml.XmlBeanDefinitionStoreException: Line 18 in XML document from class path resource [mybatis-spring.xml] is invalid; nested exception is org.xml.sax.SAXParseException; lineNumber: 18; columnNumber: 65; cvc-complex-type.2.4.c: 嚴格比對萬用字元，但是找不到元素 'util:properties' 的宣告。
		 */
		
		//啟動容器後,就可以掉用容器的getBean()
		//Q:那個bean的id是什麼?剛剛說MapperScannerConfigurer會幫我們把對象放到容器裡,
		//但那個id是什麼呢?
		//Ans:默認的id是映射器首字母小寫.
		//EmployeeDAO dao = ac.getBean("employeeDao",EmployeeDAO.class);
		//如此,這個接口內的所有方法都可以使用了.
		//Q:此處的id能不能改呢?
		//Ans:可以. [打開EmployeeDAO.java].
		//在這個接口前面加個註解就可以了.
//		EmployeeDAO dao = ac.getBean("empDao",EmployeeDAO.class);
		/**
		 * 改了mybatis-spring.xml的beans內容後,第二次執行出現以下錯誤訊息:
		 * org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'empDao' is defined
		 */
		EmployeeDAO dao = ac.getBean("empDAO",EmployeeDAO.class);
		List<Employee> e = dao.findAll();
		System.out.println(e);
		//繼改了mybatis-spring.xml裡的beans內容,以及改了上面的id後,
		//第3次執行終於出現預期結果:
		//[Employee [id=1, name=1010修改, age=107], Employee [id=3, name=學myBatis_Day2_給修改用, age=196], Employee [id=21, name=test109, age=76]]
	}
	
}
